node-mqtt-packet (9.0.1-1) unstable; urgency=low

  * debian/gbp.conf: fix format
  * New upstream version 9.0.1

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 28 Jan 2025 04:06:48 +0800

node-mqtt-packet (9.0.0-1) unstable; urgency=low

  * New upstream version 9.0.0

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 09 Dec 2023 22:18:45 +0800

node-mqtt-packet (8.1.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + node-mqtt-packet: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 07 Dec 2022 23:17:14 +0000

node-mqtt-packet (8.1.2-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 18 Sep 2022 16:56:10 +0800

node-mqtt-packet (8.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 04 Aug 2022 11:25:04 +0800

node-mqtt-packet (8.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 05 May 2022 06:14:15 +0800

node-mqtt-packet (7.1.1-1) unstable; urgency=low

  * New upstream version 7.1.1

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 28 Nov 2021 18:04:39 +0800

node-mqtt-packet (6.10.0-1) unstable; urgency=medium

  * Team upload

  [ lintian-brush ]
  * Fix field name typo in debian/upstream/metadata
    (Bugs-Database => Bug-Database)
  * Update standards version to 4.6.0, no changes needed

  [ Yadd ]
  * Bump debhelper compatibility level to 13
  * Modernize debian/watch
    * Fix GitHub tags regex
    * Fix filenamemangle
  * Use dh-sequence-nodejs
  * Drop dependency to nodejs
  * New upstream version 6.10.0
  * Refresh patch
  * Drop useless versioned constraints

 -- Yadd <yadd@debian.org>  Tue, 16 Nov 2021 10:06:20 +0100

node-mqtt-packet (6.9.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Tue, 23 Feb 2021 17:21:11 +0800

node-mqtt-packet (6.7.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 10 Dec 2020 23:53:42 +0800

node-mqtt-packet (6.6.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * New upstream release.
  * Update debian/patches/fix-buffer-usage.diff

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 19 Sep 2020 05:13:53 +0800

node-mqtt-packet (6.4.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) <paulliu@debian.org> ]
  * New upstream release.

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 31 Aug 2020 07:06:05 +0800

node-mqtt-packet (6.3.2-2) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * Add node-debug to Build-Depends

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 07 Mar 2020 12:49:52 +0800

node-mqtt-packet (6.3.2-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * New upstream version 6.3.2

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 07 Mar 2020 03:13:00 +0800

node-mqtt-packet (6.3.0-2) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.5.0
  * Add "Rules-Requires-Root: no"
  * Fix Buffer() usage
  * Wrap and sort
  * Remove duplicate test

 -- Xavier Guimard <yadd@debian.org>  Sun, 01 Mar 2020 13:12:53 +0100

node-mqtt-packet (6.3.0-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * New upstream version 6.3.0

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Mon, 03 Feb 2020 00:41:06 +0800

node-mqtt-packet (6.2.1-2) unstable; urgency=medium

  * Update standards version to 4.4.1, no changes needed.
  * Set debhelper-compat version in Build-Depends.
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Set upstream metadata fields: Bug-Submit, Bugs-Database.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.

 -- Debian Janitor <janitor@jelmer.uk>  Tue, 03 Dec 2019 02:01:21 +0000

node-mqtt-packet (6.2.1-1) unstable; urgency=low

  [ Ying-Chun Liu (PaulLiu) ]
  * New upstream version 6.2.1
  * Fix debian/tests/example-parsing.js: Don't use new Buffer().

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Wed, 11 Sep 2019 01:08:33 +0800

node-mqtt-packet (6.2.0-2) unstable; urgency=medium

  * Team upload
  * Switch install to pkg-js-tools

 -- Xavier Guimard <yadd@debian.org>  Sat, 03 Aug 2019 15:43:27 +0200

node-mqtt-packet (6.2.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * bumped debhelper & standards-version
  * Switch test to pkg-js-tools (keeping custom autopkgtest files)

 -- Suman Rajan <suman@protonmail.com>  Mon, 15 Jul 2019 15:52:50 +0530

node-mqtt-packet (6.0.0-2) unstable; urgency=medium

  * Team upload
  * Add upstream/metadata
  * Declare compliance with policy 4.3.0
  * Fix malformed subscribe crash (Closes: #928673, CVE-2019-5432)
  * Fix debian/copyright format url
  * Enable upstream test during build

 -- Xavier Guimard <yadd@debian.org>  Wed, 08 May 2019 19:27:08 +0200

node-mqtt-packet (6.0.0-1) unstable; urgency=low

  * New upstream release
  * Bump Standards-Version to 4.2.1: nothing needs to be changed
  * debian/control: Change Vcs-* to salsa
  * Bump debhelper version to 10

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 08 Nov 2018 07:26:02 +0800

node-mqtt-packet (5.5.0-1) unstable; urgency=low

  * New upstream release

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sat, 21 Apr 2018 03:58:19 +0800

node-mqtt-packet (5.4.0-2) unstable; urgency=low

  * Run benchmark in autopkgtest
  * Add more autopkgtest by some example code

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Thu, 27 Jul 2017 22:16:31 +0800

node-mqtt-packet (5.4.0-1) unstable; urgency=low

  * Initial release (Closes: #869476)

 -- Ying-Chun Liu (PaulLiu) <paulliu@debian.org>  Sun, 23 Jul 2017 10:17:09 +0000
