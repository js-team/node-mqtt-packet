var mqtt = require('mqtt-packet')
var object = {
  cmd: 'publish',
  retain: false,
  qos: 0,
  dup: false,
  length: 10,
  topic: 'test',
  payload: 'test' // Can also be a Buffer 
}
 
console.log(mqtt.generate(object))
// Prints: 
// 
// <Buffer 30 0a 00 04 74 65 73 74 74 65 73 74> 
// 
// Which is the same as: 
// 
// new Buffer.from([ 
//   48, 10, // Header (publish) 
//   0, 4, // Topic length 
//   116, 101, 115, 116, // Topic (test) 
//   116, 101, 115, 116 // Payload (test) 
// ])
